import tempfile
import om_gpx_geodecode
import om_gpx_memorials
import om_gpx_waterways
import shutil
from flask import Flask, request
import logging

app = Flask(__name__)

yandex_counter_html = """
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();
   for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }}
   k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(97098603, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/97098603" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
"""


def allowed_file(filename):
    """ Функция проверки расширения файла """
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in {'gpx'}


@app.route('/cache', methods=['GET', 'POST'])
def show_cache():
    rv = "\n<table>"
    geodata = om_gpx_geodecode.get_cached_data()
    for row in geodata:
        rv += "\n<tr><td>{} {}</td><td>{}</td></tr>".format(row[0], row[1], row[2])
    geodata = om_gpx_waterways.get_cached_data()
    for row in geodata:
        rv += "\n<tr><td>{} {}</td><td>{}</td></tr>".format(row[0], row[1], row[2])
    return rv + "\n</table>"


@app.route('/', methods=['GET', 'POST'])
def hello_world():
    html_start = """
        <html>
            <head> """ + yandex_counter_html + """
                <meta name="author" content="Oleg Machulskiy" />
                <TITLE>
                    GPX POI list generator
                </TITLE>
                <meta name="description" content="GPX POI list generator based on openstreetmap (OSM) data. After uploading your GPX file it will generate list of cities and rivers along the track" />
            </head>
            <body>
                <p>
                <form action="/" method="post" enctype="multipart/form-data">
                    Upload Your GPX file here to get track transcription:
                    <input type="file" name="file"/>
                    <input type=submit name="submit_rivers" value="Localities and Rivers" title="Click this button to generate list of localities and rivers." />
                    <input type=submit name="submit_memorials" value="Memorials" title="Click this button to see a list of memorials and historical locations within 1 kilometer along your track." />
                </form>
                </p>
            """
    html_end = """
                <p>Code: <a href="https://gitlab.com/omachulski/om-gpx-tools">https://gitlab.com/omachulski/om-gpx-tools</a>
                 Blog: <a href="https://omachulski.blogspot.com/">https://omachulski.blogspot.com/</a>
                 Youtube: <a href="https://www.youtube.com/@omachulski">https://www.youtube.com/@omachulski</a></p>
    </body>        </html>"""
    errors_text = ""

    if request.method == 'POST':

        if 'file' not in request.files:
            errors_text += 'No "file" attribute\n'
        else:
            file = request.files['file']
            if file.filename == '' or not allowed_file(file.filename):
                errors_text += 'No valid "files" attached, please upload GPX tracks only\n'
            else:
                d = tempfile.mkdtemp()
                file.save(d + "/txt.gpx")

                rv = "Source file: {}".format(file.filename)
                if "submit_memorials" in request.form:
                    rv += om_gpx_memorials.print_for_web(d + "/txt.gpx")
                else:
                    rv += om_gpx_geodecode.print_for_web(d + "/txt.gpx")
                    rv += om_gpx_waterways.print_for_web(d + "/txt.gpx")

                rv += """<p>
                Сгенерил список по трэку при помощи <a href="https://omachulski.pythonanywhere.com/">https://omachulski.pythonanywhere.com/</a>
                </p>"""

                logging.info("parsed file {}".format(rv))

                shutil.rmtree(d)

                return html_start + rv + html_end
        logging.error("error processing POST: {}".format(errors_text))
    else:
        logging.info("processing GET request")

    return html_start + """\nWHAT ?\n <h3>""" + errors_text + html_end


if __name__ == '__main__':
    app.run(debug=True, port=8080)
else:
    print(__name__)
