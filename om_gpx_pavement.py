import datetime
import sqlite3
import json
import overpy
import gpxpy
import logging
import os
import random


def set_default(obj):
    if isinstance(obj, set):
        return list(obj)
    raise TypeError("Unknown type: {} {}".format(obj, type(obj)))


class PavementReader:
    def __init__(self, gpx_filename):
        geodata_cache_sqlite = "geodata-cache.sqlite"
        self.gpx_filename = gpx_filename
        self.api = overpy.Overpass()
        self.connect = sqlite3.connect(geodata_cache_sqlite)

        gpx_file = open(gpx_filename, 'r', encoding='UTF-8')
        self.gpx = gpxpy.parse(gpx_file)
        logging.info("parsed file {}".format(self.gpx_filename))

        try:
            # self.connect.execute("""DROP TABLE  RIVERS_DATA""")
            self.connect.execute("""
                    CREATE TABLE IF NOT EXISTS PAVEMENT_DATA (
                        lat number(3, 8) ,
                        lon number(3, 8) ,
                        geodata_json TEXT
                    );
                """)
            logging.info("Success: CREATE TABLE IF NOT EXISTS PAVEMENT_DATA")
            self.connect.execute("CREATE UNIQUE INDEX IF NOT EXISTS PAVEMENT_DATA_IDX ON PAVEMENT_DATA (lat, lon)")
            logging.info("Success: CREATE UNIQUE INDEX IF NOT EXISTS PAVEMENT_DATA_IDX ON PAVEMENT_DATA")
            dataFromSql = self.connect.execute("SELECT * FROM PAVEMENT_DATA");
            for row in dataFromSql.fetchall():
                logging.debug(str(row))
                pass

        except Exception as ex:
            logging.warning("possibly table PAVEMENT_DATA already exists: {}".format(ex))

    def read_pavement_types(self, lat, lon, distance):
        query_string = """
        <bbox-query s="{:.5f}" n="{:.5f}" w="{:.5f}" e="{:.5f}"/>
        <recurse type="node-way"/>
        <query type="way">
          <item/>
          <has-kv k="highway"/>
          <has-kv k="surface"/>
        </query>
        <print/>
        """.format(lat - distance,
                   lat + distance,
                   lon - distance,
                   lon + distance)
        logging.debug(query_string)

        result = self.api.query(query_string)
        logging.debug(result)

        surface_types = set()
        for way in result.ways:
            surface_types.add(way.tags.get("surface", "n/a"))

        return surface_types

    def read_some_tags_from_track(self, lat, lon, distance):
        query_string = """
        <bbox-query s="{:.5f}" n="{:.5f}" w="{:.5f}" e="{:.5f}"/>
        <recurse type="node-way"/>
        <query type="way">
          <item/>
          <has-kv k="highway"/>
        </query>
        <print/>
        """.format(lat - distance,
                   lat + distance,
                   lon - distance,
                   lon + distance)
        logging.debug(query_string)

        result = self.api.query(query_string)
        logging.debug(result)

        surface_types = set()
        for way in result.ways:
            surface_types.add(way.tags.get("highway", "n/a"))

        return surface_types

    def get_road_pavement(self, lat, lon):
        dataFromSql = self.connect.execute("SELECT geodata_json FROM PAVEMENT_DATA WHERE lat=? and lon=?", ([lat, lon]))
        fetchall = dataFromSql.fetchall()
        set_of_surfaces = set()
        already_present_in_db_cache = False

        if len(fetchall) > 0:
            str_from_cache = fetchall[0][0]
            list_of_roads = json.loads(str_from_cache)
            set_of_surfaces = set(list_of_roads)
            already_present_in_db_cache = True

        # if random.randint(0, 1000) < 995:
        #     return []

        try:
            if not already_present_in_db_cache:
                if len(set_of_surfaces) == 0:
                    set_of_surfaces = self.read_pavement_types(lat, lon, 0.003)
                if len(set_of_surfaces) == 0:
                    set_of_surfaces = self.read_some_tags_from_track(lat, lon, 0.003)

                dumps = json.dumps(list(set_of_surfaces))
                self.connect.execute("INSERT INTO PAVEMENT_DATA (lat, lon, geodata_json) values (?,?,?)",
                                     ([lat, lon, dumps]))
                self.connect.commit()

            for surface_type in ["asphalt", "paved", "unpaved", "ground", "grass", "dirt", "track", "residential", "footway"]:
                if surface_type in set_of_surfaces:
                    return surface_type

            return str(set_of_surfaces)
        except Exception as ex:
            logging.warning("Exception : {}, returning empty set".format(ex))
            return []

    def read_all_pavement_types(self):
        pointCounter = 0
        point_counts = {}
        time_spent = {}
        previous_timestamp = None
        for track in self.gpx.tracks:
            for segment in track.segments:
                for point in segment.points:
                    try:
                        pmd = self.get_road_pavement(point.latitude, point.longitude)
                        logging.debug("get_road_pavement: {} {} : {}".format(point.latitude, point.longitude, pmd))
                        if pmd in point_counts:
                            point_counts[pmd] += 1
                        else:
                            point_counts[pmd] = 1

                        if pmd in time_spent and previous_timestamp is not None:
                            time_spent[pmd] += (point.time - previous_timestamp)
                        else:
                            if previous_timestamp is None:
                                time_spent[pmd] = datetime.timedelta()
                            else:
                                time_spent[pmd] = (point.time - previous_timestamp)

                        previous_timestamp = point.time
                        logging.debug("result = {}".format(point_counts))
                        pointCounter += 1
                        if (pointCounter % 10 == 0):
                            print("{} {} {} {} {}".format(pointCounter, point.latitude, point.longitude, point_counts,
                                                          time_spent))
                    except Exception as theException:
                        print("не будем разбираться что там за эксепшен...", theException, point)

        return point_counts


# def get_cached_data():
#     dataFromSql = sqlite3.connect("geodata-cache.sqlite").execute("SELECT distinct lat, lon, geodata_json FROM PAVEMENT_DATA order by 1")
#     return dataFromSql.fetchall()


# def print_for_web(file):
#     wr = WaterwaysReader(file)
#     all_pavement_types = wr.read_all_pavement_types()
#     result = "<pre>"
#     for i in all_pavement_types:
#         result += "{}\n".format(i)
#
#     return result + "</pre>"

if __name__ == "__main__":
    logging.basicConfig(filename='om_gpx_pavement.log', level=logging.DEBUG, filemode="w", encoding="UTF-8")

    for filename in os.listdir("."):
        logging.debug("file found: {}".format(filename))
        if filename.endswith(".gpx"):
            wr = PavementReader(filename)
            all_pavement_types = wr.read_all_pavement_types()

            print(filename)
            for i in all_pavement_types:
                print("\t{}".format(i))

        else:
            logging.debug("skip file as non GPX: {}".format(filename))

    logging.info("END")
    # print(json.dumps(finalResult, default=vars))
